# Salvation theme for Vim/Neovim

Special thanks to https://srcery-colors.github.io/ for amazing vim theme used as template.

## Instalation

### Manually - Unix-like Systems
Put `salvation.vim` in `$HOME/.vim/colors/`

### Manually - Windows
Put `salvation.vim` in `%userprofile%\vimfiles\colors\`

### Vim 8
```sh
git clone https://codeberg.org/exorcist/salvation-vim.git ~/.vim/plug/default/opt
```

### Vim-plug
```vim
Plug 'https://codeberg.org/exorcist/salvation-vim'
```

### packer.nvim
```lua
use ({
        "https://codeberg.org/exorcist/salvation-vim",
        event = "BufReadPre"
    })
```

## Supported filetype syntax
- Diff
- Html
- Xml
- Lisp dialects
- C
- Python
- CSS/SASS
- JavaScript
- YAYJS
- CoffeScript
- Ruby
- ObjectiveC
- Go
- Lua
- MoonScript
- Java
- Elixir
- Scala
- Markdown
- Haskell
- Json
- Rust
- Make

## Preview (JavaScript & Rust)
![alt text](preview/preview.png "JavaScript & Rust")
